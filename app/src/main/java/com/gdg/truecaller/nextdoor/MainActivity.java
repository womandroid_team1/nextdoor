package com.gdg.truecaller.nextdoor;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RecyclerView postsRecyclerView;
    private Button addPostBtn;
    private TextView homeScreenText;

    private final int NUM_COLUMNS = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addPostBtn = findViewById(R.id.add_btn);
        homeScreenText = findViewById(R.id.home_screen_description);
        addPostBtn.setVisibility(View.VISIBLE);
        homeScreenText.setVisibility(View.VISIBLE);

        //recyclerview
        postsRecyclerView = findViewById(R.id.recyclerview);
        postsRecyclerView.setHasFixedSize(true);
        postsRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL));


        addPostBtn.setOnClickListener(view -> {
            addPostBtn.setVisibility(View.GONE);
            homeScreenText.setVisibility(View.GONE);
            Fragment fragment = new NewPostFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack("home").commit();
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        addPostBtn.setVisibility(View.VISIBLE);
        homeScreenText.setVisibility(View.VISIBLE);

    }
}
