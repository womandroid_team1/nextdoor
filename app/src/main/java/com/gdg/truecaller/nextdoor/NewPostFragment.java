package com.gdg.truecaller.nextdoor;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;


public class NewPostFragment extends Fragment {

    private Button postBtn;
    private ListView listView;
    //Mocked categories
    private final String[] categories = new String[] {"Toys", "Clothes", "Play"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_new_post, container, false);
        listView = v.findViewById(R.id.category_list);
        postBtn = v.findViewById(R.id.post_btn);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, categories);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener((parent, view, position, id) -> Log.v("TAG", "CLICKED row number: " + position));

        postBtn.setOnClickListener(v1 -> {
            //TODO: create new PostModel instance and save it
            getFragmentManager().popBackStackImmediate("home", 0);
        });
        return v;
    }
}
