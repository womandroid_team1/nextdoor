package com.gdg.truecaller.nextdoor.Model;

public class PostModel {
        private String category;
        private String title;
        private double price;
        private String zipCode;
        private String imgLink;
        private UserModel userInfo;

        public PostModel() {

        }

        public PostModel(String category,String title, double price, String zipCode, String imgLink, UserModel userInfo) {
            this.category = category;
            this.title = title;
            this.price = price;
            this.zipCode = zipCode;
            this.imgLink = imgLink;
            this.userInfo = userInfo;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        public String getImgLink() {
            return imgLink;
        }

        public void setImgLink(String imgLink) {
            this.imgLink = imgLink;
        }

        public UserModel getUserInfo() {
            return userInfo;
        }

        public void setUserInfo(UserModel userInfo) {
            this.userInfo = userInfo;
        }

}
